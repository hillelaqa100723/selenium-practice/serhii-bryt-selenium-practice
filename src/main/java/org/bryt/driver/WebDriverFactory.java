package org.bryt.driver;

import lombok.extern.slf4j.Slf4j;
import org.bryt.utils.MyFileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.HashMap;

@Slf4j
public class WebDriverFactory {
    public static WebDriver initDriver(Browsers browser) {
        switch (browser) {
            case CHROME -> {
                log.info("Init chrome driver...");
                HashMap<String, Object> chromePrefs = new HashMap<>();
                chromePrefs.put("profile.default_content_settings.popups", 0);
                chromePrefs.put("download.default_directory", MyFileUtils.getDownloadsFolder().getAbsolutePath());

                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("prefs", chromePrefs);
                log.info("Chrome driver init is successful");
                return new ChromeDriver(options);
            }
            case FIREFOX -> {
                return new FirefoxDriver();
            }
            case EDGE -> {
                return new EdgeDriver();
            }
        }
        return null;
    }

    public static WebDriver initDriver() {
        String browserToRun = System.getProperty("browserToRun", "chrome");
        try {
            Browsers browser = Browsers.valueOf(browserToRun.toUpperCase());
            return initDriver(browser);
        } catch (IllegalArgumentException e) {
            System.err.println(browserToRun + " is not supported yet!!!");
            System.exit(-1);
        }
        return null;
    }
}
