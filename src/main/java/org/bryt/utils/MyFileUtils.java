package org.bryt.utils;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Date;
import java.util.List;

import static org.apache.commons.io.ThreadUtils.sleep;

@Slf4j
public class MyFileUtils {
    public static File generateFile(File target, int sentencesCount) {
        Faker faker = new Faker();
        List<String> sentences = faker.lorem().sentences(sentencesCount);
        try {
            FileUtils.writeLines(target, sentences);
            return target;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static File prepareFile(File template) {
        File target = new File(template.getParent(), "file_" + new Date().getTime() + ".txt");
        try {
            FileUtils.copyFile(template, target);
            target.deleteOnExit();
            return target;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static File getFileFromResources(String fileName) {
        return new File(MyFileUtils.class.getClassLoader().getResource(fileName).getFile());
    }

    public static File getDownloadsFolder() {
        return new File(Constants.DOWNLOAD_FOLDER);
    }

    public static void prepareDownloadsFolder() {
        log.info("Init download folder!!!");
        if (!getDownloadsFolder().exists()) {
            getDownloadsFolder().mkdir();
        } else {
            try {
                FileUtils.cleanDirectory(getDownloadsFolder());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static File getScreenshotFolder() {
        return new File(Constants.SCREENSHOT_FOLDER);
    }

    public static void prepareScreenshotFolder() {
        log.info("Init screenshots folder!!!");
        if (!getScreenshotFolder().exists()) {
            getScreenshotFolder().mkdir();
        } else {
            try {
                FileUtils.cleanDirectory(getScreenshotFolder());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void waitTillFileIsDownloaded(File file) {
        int timeout = 60;
        int count = 0;

        while (!file.exists() && count < timeout) {
            try {
                sleep(Duration.ofSeconds(3));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            count += 3;
        }

        count = 0;

        while (count < timeout){
            long lengthBefore = file.length();
            try {
                sleep(Duration.ofSeconds(3));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            long lengthAfter = file.length();
            if (lengthBefore == lengthAfter){
                 return;
            }
            count+=3;
        }
    }


}
