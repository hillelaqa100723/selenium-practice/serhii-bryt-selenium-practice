package org.bryt.utils;

public class Constants {
    public static final String DOWNLOAD_FOLDER = "downloads";
    public static final String SCREENSHOT_FOLDER = "screenshots";
}
