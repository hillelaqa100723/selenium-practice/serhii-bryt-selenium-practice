package org.bryt.pages;

import io.qameta.allure.Step;
import jdk.net.ExtendedSocketOptions;
import lombok.extern.slf4j.Slf4j;
import org.bryt.driver.WebDriverHolder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Slf4j
public class LoggedInPage extends BasePage {

    @FindBy(css = ".button.radius")
    private WebElement logoutButton;

    @Step
    public LoginPage logout() {
        log.info("Logout");
        logoutButton.click();
        return new LoginPage();
    }

    public LoggedInPage() {
        super();
        waitForPageIsLoaded();
    }

    @Step
    private void waitForPageIsLoaded() {
        log.info("Waiting for page is loaded...");
        WebDriverWait wait = WebDriverHolder.getInstance().getWait();
        wait.until(ExpectedConditions.visibilityOf(successMessage));
    }


}
