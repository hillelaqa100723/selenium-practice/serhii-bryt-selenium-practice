package org.bryt.pages;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.bryt.driver.WebDriverHolder;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Slf4j
public class BasePage {

    @FindBy(css = ".flash.success")
    protected WebElement successMessage;

    public BasePage() {
        PageFactory.initElements(WebDriverHolder.getInstance().getDriver(), this);
    }

    @Step
    public boolean isSuccessMessageVisible() {
        log.info("Is Success Message Visible ");
        return successMessage.isDisplayed();
    }

    @Step
    public String getSuccessMessageText() {
        log.info("Getting success message text");
        return successMessage.getText()
                .replace("×", "")
                .trim();
    }
}
