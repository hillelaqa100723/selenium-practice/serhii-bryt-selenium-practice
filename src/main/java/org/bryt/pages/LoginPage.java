package org.bryt.pages;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.bryt.driver.WebDriverHolder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Slf4j
public class LoginPage extends BasePage{

    @FindBy(id = "username")
    private WebElement nameInput;

    @FindBy(name = "password")
    private WebElement passwordInput;

    @FindBy(css = ".radius")
    private WebElement loginButton;


    @FindBy(css = ".flash.error")
    private WebElement errorMessage;

    @Step
    private LoginPage enterName(String name) {
        nameInput.clear();
        nameInput.sendKeys(name);
        return this;
    }

    @Step
    private LoginPage enterPassword(String password) {
        passwordInput.clear();
        passwordInput.sendKeys(password);
        return this;
    }

    @Step
    private void clickLoginButton() {
        loginButton.click();
    }

    @Step
    public LoggedInPage login(String userName, String userPass) {
        log.info("Login with name: {} and password: {}", userName, userPass);
        enterName(userName)
                .enterPassword(userPass)
                .clickLoginButton();
        return new LoggedInPage();
    }

    @Step
    public LoginPage loginUnsuccess(String userName, String userPass) {
        log.info("Login with name: {} and password: {}", userName, userPass);
        enterName(userName)
                .enterPassword(userPass)
                .clickLoginButton();
        return new LoginPage();
    }



    @Step
    public boolean isErrorMessageVisible() {
        log.info("Check if error message is visible");
        return errorMessage.isDisplayed();
    }

    @Step
    public String getErrorMessageText() {
        log.info("Getting error message text");
        return errorMessage.getText()
                .replace("×", "")
                .trim();
    }



}
