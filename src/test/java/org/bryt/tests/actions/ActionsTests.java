package org.bryt.tests.actions;

import org.bryt.tests.base.BaseTestClassForPractice;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class ActionsTests extends BaseTestClassForPractice {

    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/hovers");
    }

    @Test
    public void hoverTest() {
        List<WebElement> elements = driver.findElements(By.cssSelector(".figure"));
        for (int i = 0; i < 3; i++) {
            WebElement webElement = elements.get(i);
            new Actions(driver).moveToElement(webElement).build().perform();
            pause(3000);
            WebElement element = webElement.findElement(By.xpath(".//h5"));
            String text = element.getText();
            Assert.assertEquals(text, "name: user" + (i + 1));
        }
    }

    public void pause(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
