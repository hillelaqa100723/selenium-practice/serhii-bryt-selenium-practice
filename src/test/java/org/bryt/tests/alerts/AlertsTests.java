package org.bryt.tests.alerts;

import org.bryt.tests.base.BaseTestClassForPractice;
import org.bryt.utils.PropertyReader;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class AlertsTests extends BaseTestClassForPractice {

    @BeforeMethod
    public void beforeMethod() {
        openUrl("/javascript_alerts");
    }


    @Test(dataProvider = "dataProvider")
    public void dataDrivenTest(Buttons button, String alertText, String resultText){
        clickJSButton(button);
        String textOnAlert = workWithAlert(true);
        String resultText1 = getResultText();
        Assert.assertEquals(textOnAlert, alertText);
        Assert.assertEquals(resultText1, resultText);
    }

    @DataProvider
    public Object[][] dataProvider(){
        return new Object[][]{
                {Buttons.JS_ALERT, "I am a JS Alert", "You successfully clicked an alert"},
                {Buttons.JS_CONFIRM, "I am a JS Confirm", "You clicked: Ok"},
                {Buttons.JS_PROMPT, "I am a JS prompt", "You entered:"}
        };
    }

    @Test
    public void jsAlertTest() {
        String expectedTextAtAlert = "I am a JS Alert";
        String expectedResultText = "You successfully clicked an alert";

        clickJSButton(Buttons.JS_ALERT);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);

    }

    @Test
    public void jsAlertTestWithJS() {
        String expectedTextAtAlert = "I am a JS Alert";
        String expectedResultText = "You successfully clicked an alert";

        clickJSButtonCallingJs(Buttons.JS_ALERT);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);

    }

    @Test
    public void jsAlertTestWithJS1() {
        String expectedTextAtAlert = "I am a JS Alert";
        String expectedResultText = "You successfully clicked an alert";

        clickJSButtonWithJs(Buttons.JS_ALERT);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultTextWithJs(), expectedResultText);

    }


    @Test
    public void jsPromptTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Hillel School!";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + testToEnter, resultText);
    }

    private void clickJSButton(Buttons buttons) {
        findButton(buttons).click();
    }

    private void clickJSButtonCallingJs(Buttons buttons) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("return " + buttons.getCode());
    }

    private void clickJSButtonWithJs(Buttons buttons) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        WebElement button = findButton(buttons);
        executor.executeScript("return arguments[0].click();", button);
    }


    private WebElement findButton(Buttons button) {
        return driver.findElement(By.xpath("//button[text()='%s']".formatted(button.getTextValue())));
    }

    private String workWithAlert(boolean accept, String... textToInput) {
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();

        if (textToInput.length > 0) {
            alert.sendKeys(textToInput[0]);
        }

        if (accept) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return text;
    }

    private String getResultText() {
        return driver.findElement(By.id("result")).getText();
    }

    private String getResultTextWithJs() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.id("result"));
        return executor.executeScript("return arguments[0].textContent;", element).toString();
    }


    public enum Buttons {
        JS_ALERT("Click for JS Alert", "jsAlert()"),
        JS_CONFIRM("Click for JS Confirm", "jsConfirm()"),
        JS_PROMPT("Click for JS Prompt", "jsPrompt()");

        private String textValue;
        private String code;

        Buttons(String textValue, String code) {
            this.textValue = textValue;
            this.code = code;
        }

        public String getTextValue() {
            return textValue;
        }

        public String getCode() {
            return code;
        }
    }
}
