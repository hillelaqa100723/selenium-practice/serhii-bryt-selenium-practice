package org.bryt.tests.frames;

import org.bryt.tests.base.BaseTestClassForPractice;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FramesTests extends BaseTestClassForPractice {
    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/nested_frames");
    }

    @Test
    public void frameInFrame() {
        driver.switchTo()
                .frame("frame-top")
                .switchTo()
                .frame("frame-left");
        String text = driver.findElement(By.tagName("body")).getText().trim();
        Assert.assertEquals(text, "LEFT");
    }
}
