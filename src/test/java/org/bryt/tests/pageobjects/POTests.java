package org.bryt.tests.pageobjects;

import com.aventstack.extentreports.testng.listener.ExtentITestListenerAdapter;
import org.bryt.pages.LoggedInPage;
import org.bryt.pages.LoginPage;
import org.bryt.tests.base.BaseTestClassForPractice;
import org.bryt.tests.base.BaseTestClassForPractice1;
import org.bryt.utils.PropertyReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

public class POTests extends BaseTestClassForPractice1 {

    @Test
    public void locatorTest() {
        openUrl("/login");

        new LoginPage()
                .login(PropertyReader.getInstance().getProperty("userName"),
                        PropertyReader.getInstance().getProperty("userPass"))
                .logout()
                .isErrorMessageVisible();
    }

    @Test(enabled = false)
    public void locatorTest1() {
        openUrl("/login");

        new LoginPage()
                .loginUnsuccess(PropertyReader.getInstance().getProperty("userName") + "1",
                        PropertyReader.getInstance().getProperty("userPass"));

    }

    @Test
    public void locatorTest2() {
        openUrl("/login");

        LoginPage loginPage = new LoginPage();
        loginPage = loginPage
                .loginUnsuccess("", "");
        Assert.assertTrue(loginPage.isErrorMessageVisible());

    }

    @Test
    public void locatorTest3() {
        String msgText = "You logged out of the secure area!";
        String msgText1 = "You logged into a secure area!";

        openUrl("/login");

        LoginPage loginPage = new LoginPage();
        LoggedInPage loggedInPage = loginPage
                .login(PropertyReader.getInstance().getProperty("userName"),
                        PropertyReader.getInstance().getProperty("userPass"));

        Assert.assertTrue(loggedInPage.isSuccessMessageVisible());
        Assert.assertEquals(loggedInPage.getSuccessMessageText(), msgText1);


        loginPage = loggedInPage
                .logout();

        Assert.assertTrue(loginPage.isSuccessMessageVisible());
        Assert.assertEquals(loginPage.getSuccessMessageText(), msgText);

    }
}
