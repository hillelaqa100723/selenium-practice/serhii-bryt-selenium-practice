package org.bryt.tests.base;

import com.aventstack.extentreports.testng.listener.ExtentITestListenerAdapter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.bryt.driver.WebDriverFactory;
import org.bryt.driver.WebDriverHolder;
import org.bryt.listeners.MyTestsListener;
import org.bryt.utils.MyFileUtils;
import org.bryt.utils.PropertyReader;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import java.io.File;
import java.io.IOException;
import java.util.Date;

@Slf4j
@Listeners({MyTestsListener.class, ExtentITestListenerAdapter.class})
public class BaseTestClassForPractice1 {


    @BeforeSuite
    public void beforeClass() {
        MyFileUtils.prepareDownloadsFolder();
        MyFileUtils.prepareScreenshotFolder();
        WebDriverHolder.getInstance();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        WebDriverHolder.getInstance().quitDriver();
    }

    protected void openUrl(String url) {
        String baseUrl = PropertyReader.getInstance().getProperty("baseUrl");
        if (url.startsWith("/")) {
            log.info("Open {} url", baseUrl + url);
            WebDriverHolder.getInstance().getDriver().get(baseUrl + url);
        } else {
            log.info("Open {} url", url);
            WebDriverHolder.getInstance().getDriver().get(url);
        }
    }

//    @AfterMethod
//    public void afterMethodFailCheck(ITestResult testResult) throws IOException {
//        WebDriver driver = WebDriverHolder.getInstance().getDriver();
//        if (testResult.getStatus() == ITestResult.FAILURE) {
//            File screenshotAs = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//            FileUtils.copyFile(screenshotAs, new File("screenshots/screenshots_" + testResult.getMethod() + "_" + new Date().getTime() + ".png"));
//        }
//    }
}
