package org.bryt.tests.base;

import org.bryt.driver.Browsers;
import org.bryt.driver.WebDriverFactory;
import org.bryt.utils.MyFileUtils;
import org.bryt.utils.PropertyReader;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseTestClassForPractice {
    protected WebDriver driver;

    @BeforeSuite
    public void beforeClass() {
        MyFileUtils.prepareDownloadsFolder();
        driver = WebDriverFactory.initDriver();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    protected void openUrl(String url) {
        String baseUrl = PropertyReader.getInstance().getProperty("baseUrl");
        if (url.startsWith("/"))
            driver.get(baseUrl + url);
        else driver.get(url);
    }
}
