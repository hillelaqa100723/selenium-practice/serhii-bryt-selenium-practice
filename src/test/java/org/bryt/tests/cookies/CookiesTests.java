package org.bryt.tests.cookies;

import org.bryt.tests.base.BaseTestClassForPractice;
import org.bryt.utils.PropertyReader;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Set;

public class CookiesTests extends BaseTestClassForPractice {

    @BeforeMethod
    public void beforeMethod() {
        driver.get(PropertyReader.getInstance().getProperty("baseUrl") + "/login");
    }

    @Test
    public void cookiesTest() {

        WebElement userNameInput = driver.findElement(By.xpath("//input[@id='username']"));
        WebElement passwordInput = driver.findElement(By.xpath("//input[@id='password']"));

        userNameInput.clear();
        userNameInput.sendKeys(PropertyReader.getInstance().getProperty("userName"));

        passwordInput.clear();
        passwordInput.sendKeys(PropertyReader.getInstance().getProperty("userPass"));

        WebElement loginButton = driver.findElement(By.xpath("//button[@class='radius']"));
        loginButton.submit();

        Set<Cookie> cookies = driver.manage().getCookies();
        for (Cookie cookie : cookies) {
            System.out.println(cookie.getName() + " ==> " + cookie.getValue());
        }

        System.out.println();

        Cookie myCookie = new Cookie("MyCookie", "MuCookieValue");
        driver.manage().addCookie(myCookie);

        cookies = driver.manage().getCookies();
        for (Cookie cookie : cookies) {
            System.out.println(cookie.getName() + " ==> " + cookie.getValue());
        }

        System.out.println();

        driver.manage().deleteCookieNamed(myCookie.getName());

        cookies = driver.manage().getCookies();
        for (Cookie cookie : cookies) {
            System.out.println(cookie.getName() + " ==> " + cookie.getValue());
        }

        System.out.println();
        driver.manage().deleteAllCookies();

        cookies = driver.manage().getCookies();
        for (Cookie cookie : cookies) {
            System.out.println(cookie.getName() + " ==> " + cookie.getValue());
        }

        driver.navigate().refresh();

        userNameInput = driver.findElement(By.xpath("//input[@id='username']"));
        passwordInput = driver.findElement(By.xpath("//input[@id='password']"));

        Assert.assertTrue(userNameInput.isDisplayed());
        Assert.assertTrue(passwordInput.isDisplayed());

    }
}
