package org.bryt.tests.locators;

import org.bryt.tests.base.BaseTestClassForPractice;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LocatorsTests extends BaseTestClassForPractice {

    @Test
    public void locatorTest() {
        final String userName = "tomsmith";
        final String userPass = "SuperSecretPassword!";


        driver.get("http://the-internet.herokuapp.com/login");

        //input[@id='username']

        WebElement userNameInput = driver.findElement(By.xpath("//input[@id='username']"));
        WebElement passwordInput = driver.findElement(By.xpath("//input[@id='password']"));

        userNameInput.clear();
        userNameInput.sendKeys(userName);

        passwordInput.clear();
        passwordInput.sendKeys(userPass);

        WebElement loginButton = driver.findElement(By.xpath("//button[@class='radius']"));
        loginButton.submit();

        WebElement logoutButton = driver.findElement(By.xpath("//a[contains(@class,'radius')]"));

        Assert.assertTrue(logoutButton.isDisplayed());

        logoutButton.click();

        userNameInput = driver.findElement(By.xpath("//input[@id='username']"));
        passwordInput = driver.findElement(By.xpath("//input[@id='password']"));

        Assert.assertTrue(userNameInput.isDisplayed());
        Assert.assertTrue(passwordInput.isDisplayed());

    }
}
