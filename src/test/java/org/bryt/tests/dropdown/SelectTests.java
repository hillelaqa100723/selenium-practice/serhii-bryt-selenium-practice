package org.bryt.tests.dropdown;

import org.bryt.tests.base.BaseTestClassForPractice;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class SelectTests extends BaseTestClassForPractice {
    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/dropdown");
    }

    @Test
    public void selectTest(){
        WebElement element = driver.findElement(By.cssSelector("#dropdown"));
        Select select = new Select(element);

        select.selectByVisibleText("Option 2");
        String value = select.getFirstSelectedOption().getAttribute("value");
        Assert.assertEquals(value, "2");

        select.selectByValue("1");
        String text = select.getFirstSelectedOption().getText();
        Assert.assertEquals(text, "Option 1");

        List<WebElement> options = select.getOptions();

        for (WebElement option: options){
            System.out.println(option.getAttribute("value") + " ==> " + option.getText());
        }
    }
}
