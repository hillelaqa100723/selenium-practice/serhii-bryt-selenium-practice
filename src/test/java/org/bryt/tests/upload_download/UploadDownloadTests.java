package org.bryt.tests.upload_download;

import org.apache.commons.io.FileUtils;
import org.bryt.tests.base.BaseTestClassForPractice;
import org.bryt.utils.MyFileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;

public class UploadDownloadTests extends BaseTestClassForPractice {
    @Test
    public void uploadFiles(){
        driver.get("http://the-internet.herokuapp.com/upload");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        File preparedFile = MyFileUtils.prepareFile(MyFileUtils.getFileFromResources("template.txt"));
        driver.findElement(By.id("file-upload")).sendKeys(preparedFile.getAbsolutePath());
        driver.findElement(By.id("file-submit")).click();
        //uploaded-files

        WebElement uploadFilesElement = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("uploaded-files"))));

        Assert.assertEquals(uploadFilesElement.getText().trim(), preparedFile.getName());
    }

    @Test
    public void downloadFile() throws IOException {
        driver.get("http://the-internet.herokuapp.com/upload");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        File preparedFile = MyFileUtils.prepareFile(MyFileUtils.getFileFromResources("template.txt"));
        driver.findElement(By.id("file-upload")).sendKeys(preparedFile.getAbsolutePath());
        driver.findElement(By.id("file-submit")).click();        //uploaded-files
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("uploaded-files"))));

        driver.get("http://the-internet.herokuapp.com/download");
        driver.findElement(By.linkText(preparedFile.getName())).click();

        File downloadedFile = new File(MyFileUtils.getDownloadsFolder(), preparedFile.getName());
        MyFileUtils.waitTillFileIsDownloaded(downloadedFile);

       Assert.assertTrue(FileUtils.contentEquals(preparedFile, downloadedFile));

    }
}
