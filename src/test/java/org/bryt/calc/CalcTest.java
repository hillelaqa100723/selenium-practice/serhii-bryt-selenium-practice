package org.bryt.calc;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalcTest {
    Calculator calculator = new Calculator();

    @AfterMethod
    public void clearCalc(){
        calculator.clear();
    }

    @Test(dataProvider = "dataProvider")
    public void calcPlus(Double operand1, Double operand2, Double expResult){
        Double result = calculator
                .operand1(operand1)
                .plus()
                .operand2(operand2)
                .equal()
                .getResult();
        Assert.assertEquals(result, expResult, "Smth went wrong!!!");
    }

    @DataProvider
    public Object[][] dataProvider(){

        return new Object[][]{
                {2.0, 2.0, 4.0},
                {3.5, 7.0, 10.5},
                {5.0, 6.0, 12.0},
                {-4.0, 10.0, 6.0},
                {10.0, -40.0, -30.0}
        };
    }
}
