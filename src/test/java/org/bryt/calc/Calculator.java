package org.bryt.calc;

public class Calculator {

    private Double operand1;
    private Double operand2;

    private Operation operation;

    Double result;

    public Calculator operand1(Double operand1){
        this.operand1 = operand1;
        return this;
    }

    public Calculator operand2(Double operand2){
        this.operand2 = operand2;
        return this;
    }

    public Calculator plus(){
        operation = Operation.PLUS;
        return this;
    }

    public Calculator minus(){
        operation = Operation.MINUS;
        return this;
    }

    public Calculator mult(){
        operation = Operation.MULT;
        return this;
    }

    public Calculator div(){
        operation = Operation.DIV;
        return this;
    }

    public Calculator equal(){
        calculate();
        return this;
    }

    public Double getResult(){
        return result;
    }

    public Calculator clear(){
        operand1 = 0.0;
        operand2 = 0.0;
        result = 0.0;
        operation = Operation.PLUS;
        return this;
    }

    private void calculate() {
        switch (operation) {
            case PLUS -> result = operand1 + operand2;
            case MULT -> result = operand1 * operand2;
            case MINUS -> result = operand1 - operand2;
            case DIV -> result = operand1 / operand2;
        }
    }


    enum Operation{
        PLUS,
        MINUS,
        DIV,
        MULT
    }
}
